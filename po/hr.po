# SOME DESCRIPTIVE TITLE.
# Copyright (C) YEAR THE PACKAGE'S COPYRIGHT HOLDER
# This file is distributed under the same license as the PACKAGE package.
# 
# Translators:
# Edin Veskovic <edin.lockedin@gmail.com>, 2018
# Edin Veskovic <edin.lockedin@gmail.com>, 2017
# Edin Veskovic <edin.lockedin@gmail.com>, 2014
# Ivica  Kolić <ikoli@yahoo.com>, 2013,2017
# Lovro Kudelić <lovro.kudelic@outlook.com>, 2017
msgid ""
msgstr ""
"Project-Id-Version: Xfce Panel Plugins\n"
"Report-Msgid-Bugs-To: \n"
"POT-Creation-Date: 2021-07-22 00:48+0200\n"
"PO-Revision-Date: 2021-07-21 22:48+0000\n"
"Last-Translator: Xfce Bot <transifex@xfce.org>\n"
"Language-Team: Croatian (http://www.transifex.com/xfce/xfce-panel-plugins/language/hr/)\n"
"MIME-Version: 1.0\n"
"Content-Type: text/plain; charset=UTF-8\n"
"Content-Transfer-Encoding: 8bit\n"
"Language: hr\n"
"Plural-Forms: nplurals=3; plural=n%10==1 && n%100!=11 ? 0 : n%10>=2 && n%10<=4 && (n%100<10 || n%100>=20) ? 1 : 2;\n"

#. initialize value label widget
#: ../panel-plugin/sensors-plugin.c:342 ../panel-plugin/sensors-plugin.c:457
#: ../panel-plugin/sensors-plugin.c:977
msgid "<span><b>Sensors</b></span>"
msgstr "<span><b>Senzori</b></span>"

#. output to stdout on command line, not very useful for user, except for
#. tracing problems
#: ../panel-plugin/sensors-plugin.c:854
#, c-format
msgid ""
"Sensors Plugin:\n"
"Seems like there was a problem reading a sensor feature value.\n"
"Proper proceeding cannot be guaranteed.\n"
msgstr "Priključak za senzore:\nPostoji problem pri čitanju vrijednosti senzora.\nPravilan nastavak nije garantiran.\n"

#: ../panel-plugin/sensors-plugin.c:884
msgid "No sensors selected!"
msgstr "NIjedan senzor nije odabran!"

#: ../panel-plugin/sensors-plugin.c:1720
msgid "UI style:"
msgstr "UI stil:"

#: ../panel-plugin/sensors-plugin.c:1721
msgid "_text"
msgstr "_tekst"

#: ../panel-plugin/sensors-plugin.c:1722
msgid "_progress bars"
msgstr "_trake napretka"

#: ../panel-plugin/sensors-plugin.c:1723
msgid "_tachos"
msgstr "_tahometri"

#: ../panel-plugin/sensors-plugin.c:1757
msgid "Show _labels"
msgstr "Pokazuj _natpise"

#: ../panel-plugin/sensors-plugin.c:1779
msgid "_Automatic bar colors"
msgstr ""

#: ../panel-plugin/sensors-plugin.c:1781
msgid ""
"If enabled, bar colors depend on their values (normal, high, very high).\n"
"If disabled, bars use the user-defined sensor colors.\n"
"If a particular user-defined sensor color is unspecified,\n"
"the bar color is derived from the current UI style."
msgstr ""

#: ../panel-plugin/sensors-plugin.c:1808
msgid "_Show title"
msgstr "_Pokaži naslov"

#: ../panel-plugin/sensors-plugin.c:1827
msgid "_Number of text lines:"
msgstr "_Broj redaka teksta:"

#. The Xfce 4 panel can have several rows or columns. With such a mode,
#. the plugins are allowed to span over all available rows/columns.
#. When translating, "cover" might be replaced by "use" or "span".
#: ../panel-plugin/sensors-plugin.c:1859
msgid "_Cover all panel rows/columns"
msgstr "_Prekrij sve retke/stupce ploče"

#: ../panel-plugin/sensors-plugin.c:1879
msgid "F_ont size:"
msgstr "V_eličina fonta:"

#: ../panel-plugin/sensors-plugin.c:1885
msgid "x-small"
msgstr "x-mali"

#: ../panel-plugin/sensors-plugin.c:1886
msgid "small"
msgstr "mali"

#: ../panel-plugin/sensors-plugin.c:1887
msgid "medium"
msgstr "srednji"

#: ../panel-plugin/sensors-plugin.c:1888
msgid "large"
msgstr "veliki"

#: ../panel-plugin/sensors-plugin.c:1889
msgid "x-large"
msgstr "x-veliki"

#: ../panel-plugin/sensors-plugin.c:1913
msgid "F_ont:"
msgstr "F_ont:"

#: ../panel-plugin/sensors-plugin.c:1938
msgid "Show _Units"
msgstr "Pokaži _jedinice"

#: ../panel-plugin/sensors-plugin.c:1956
msgid "Small horizontal s_pacing"
msgstr "Mali vodoravni r_azmak"

#. Alpha value of the tacho coloring
#: ../panel-plugin/sensors-plugin.c:1979
msgid "Tacho color alpha value:"
msgstr "Alfa vrijednost boje pokazivača:"

#. The value from HSV color model
#: ../panel-plugin/sensors-plugin.c:1992
msgid "Tacho color value:"
msgstr "Vrijednost boje pokazivača:"

#: ../panel-plugin/sensors-plugin.c:2017
msgid "Suppress messages"
msgstr "Potisni poruke"

#: ../panel-plugin/sensors-plugin.c:2032
msgid "Suppress tooltip"
msgstr "Potisni oblačić"

#: ../panel-plugin/sensors-plugin.c:2052
msgid "E_xecute on double click:"
msgstr "I_zvrši nakon dvostrukog klika:"

#: ../panel-plugin/sensors-plugin.c:2079
msgid "_View"
msgstr "_Pogled"

#: ../panel-plugin/sensors-plugin.c:2092
msgid "UI style options"
msgstr "Opcije UI stila"

#: ../panel-plugin/sensors-plugin.c:2121
msgid "_Miscellaneous"
msgstr "_Razno"

#: ../panel-plugin/sensors-plugin.c:2175 ../panel-plugin/sensors-plugin.h:31
#: ../lib/hddtemp.c:151
msgid "Sensors Plugin"
msgstr "Priključak senzora"

#: ../panel-plugin/sensors-plugin.c:2183
msgid "Properties"
msgstr "Svojstva"

#: ../panel-plugin/sensors-plugin.c:2209
msgid ""
"You can change a feature's properties such as name, colours, min/max value "
"by double-clicking the entry, editing the content, and pressing \"Return\" "
"or selecting a different field."
msgstr "Svojstva značajki mjenjajte tako da dva puta kliknete na liniju, zatim uredite sadržaj i pritisnite Enter ili izaberite drugo polje."

#: ../panel-plugin/sensors-plugin.c:2292
msgid "Show sensor values from LM sensors, ACPI, hard disks, NVIDIA"
msgstr "Pokazuje vrijednosti senzora LM senzora, ACPI senzora, tvrdih diskova, NVIDIA uređaja"

#: ../panel-plugin/sensors-plugin.c:2294
msgid "Copyright (c) 2004-2021\n"
msgstr ""

#. only use this if no hddtemp sensor
#. or do only use this , if it is an lmsensors device. whatever.
#: ../lib/configuration.c:163 ../lib/hddtemp.c:358
msgid "Hard disks"
msgstr "Tvrdi diskovi"

#. Note for translators: As some laptops have several batteries such as the
#. T440s,
#. there might be some perturbation with the battery name here and BAT0/BAT1
#. for
#. power/voltage. So we prepend BAT0/1 to the battery name as well, with the
#. result
#. being something like "BAT1 - 45N1127". Users can then rename the batteries
#. to
#. their own will while keeping consistency to their power/voltage features.
#. You might want to format this with a hyphen and without spacing, or with a
#. dash; the result might be BAT1–Power or whatever fits your language most.
#. Spaces allow line breaks over the tachometers.
#. You might want to format this with a hyphen and without spacing, or with a
#. dash; the result might be BAT1–Voltage or whatever fits your language most.
#. Spaces allow line breaks over the tachometers.
#: ../lib/acpi.c:317 ../lib/acpi.c:622 ../lib/acpi.c:690
#, c-format
msgid "%s - %s"
msgstr "%s - %s"

#. Power with unit Watts, not Energy with Joules or kWh
#: ../lib/acpi.c:624
msgid "Power"
msgstr "Snaga"

#: ../lib/acpi.c:690
msgid "Voltage"
msgstr "Naponi"

#: ../lib/acpi.c:744 ../lib/acpi.c:756
msgid "ACPI"
msgstr "ACPI"

#: ../lib/acpi.c:747
#, c-format
msgid "ACPI v%s zones"
msgstr "ACPI v%s zone"

#: ../lib/acpi.c:888
msgid "<Unknown>"
msgstr "<Nepoznato>"

#: ../lib/nvidia.c:64
msgid "NVIDIA GPU core temperature"
msgstr ""

#: ../lib/nvidia.c:65
msgid "nvidia"
msgstr "nvidia"

#: ../lib/hddtemp.c:110
msgid "Don't show this message again"
msgstr ""

#: ../lib/hddtemp.c:357
msgid "S.M.A.R.T. harddisk temperatures"
msgstr "S.M.A.R.T. temperature tvrdih diskova"

#: ../lib/hddtemp.c:527
#, c-format
msgid ""
"\"hddtemp\" was not executed correctly, although it is executable. This is most probably due to the disks requiring root privileges to read their temperatures, and \"hddtemp\" not being setuid root.\n"
"\n"
"An easy but dirty solution is to run \"chmod u+s %s\" as root user and restart this plugin or its panel.\n"
"\n"
"Calling \"%s\" gave the following error:\n"
"%s\n"
"with a return value of %d.\n"
msgstr "\"hddtemp\" nije pravilno izvršen iako je izvršan. Ovo se vjerojatno događa zato što su diskovima potrebne root privilegije za čitanje temperature, a \"hddtemp\" nema te privilegije.\n\nLakši i prljaviji način je da izvršite \"chmod u+s %s\" naredbu kao root korisnik i ponovno pokrenete dodatak.\n\nPozivanje \"%s\" dalo je slijedeću grešku:\n%s\ns povratnom vrijednosti %d.\n"

#: ../lib/hddtemp.c:541 ../lib/hddtemp.c:564
msgid "Suppress this message in future"
msgstr "Potisni ovu poruku u budućnosti"

#: ../lib/hddtemp.c:559
#, c-format
msgid ""
"An error occurred when executing \"%s\":\n"
"%s"
msgstr "Došlo oje do greške pri izvršavanju \"%s\":\n%s"

#: ../lib/lmsensors.c:63
msgid "LM Sensors"
msgstr "LM senzori"

#: ../lib/lmsensors.c:297
#, c-format
msgid "Error: Could not connect to sensors!"
msgstr "Greška: Neuspjelo povezivanje sa senzorima!"

#: ../lib/sensors-interface.c:79
msgid "Sensors Plugin Failure"
msgstr "Dodatak za senzore neuspješan"

#: ../lib/sensors-interface.c:80
msgid ""
"Seems like there was a problem reading a sensor feature value.\n"
"Proper proceeding cannot be guaranteed."
msgstr "Postoji problem pri čitanju vrijednosti senzora.\nPravilan nastavak nije garantiran. "

#: ../lib/sensors-interface.c:148
msgid "Sensors t_ype:"
msgstr "T_ip senzora:"

#: ../lib/sensors-interface.c:167
msgid "Description:"
msgstr "Opis:"

#: ../lib/sensors-interface.c:196
msgid "U_pdate interval (seconds):"
msgstr "I_nterval ažuriranja (sekunde):"

#: ../lib/sensors-interface.c:231
msgid "Name"
msgstr "Ime"

#: ../lib/sensors-interface.c:242
msgid "Value"
msgstr "Vrijednost"

#: ../lib/sensors-interface.c:248
msgid "Show"
msgstr "Pokaži"

#: ../lib/sensors-interface.c:257
msgid "Color"
msgstr "Boja"

#: ../lib/sensors-interface.c:268
msgid "Min"
msgstr "Min"

#: ../lib/sensors-interface.c:279
msgid "Max"
msgstr "Maks"

#: ../lib/sensors-interface.c:311
msgid "Temperature scale:"
msgstr "Temperaturna skala:"

#: ../lib/sensors-interface.c:312
msgid "_Celsius"
msgstr "_Celzijus"

#: ../lib/sensors-interface.c:314
msgid "_Fahrenheit"
msgstr "_Fahrenheit"

#: ../lib/sensors-interface.c:346
msgid "_Sensors"
msgstr "_Senzori"

#: ../lib/sensors-interface-common.c:70 ../lib/sensors-interface-common.c:71
msgid "No sensors found!"
msgstr "NIsu nađeni senzori!"

#: ../lib/sensors-interface-common.c:138
#, c-format
msgid "%.0f °F"
msgstr "%.0f °F"

#: ../lib/sensors-interface-common.c:140
#, c-format
msgid "%.0f °C"
msgstr "%.0f °C"

#: ../lib/sensors-interface-common.c:144
#, c-format
msgid "%+.3f V"
msgstr "%+.3f V"

#: ../lib/sensors-interface-common.c:148
#, c-format
msgid "%+.3f A"
msgstr "%+.3f A"

#: ../lib/sensors-interface-common.c:152
#, c-format
msgid "%.0f mWh"
msgstr "%.0f mWh"

#: ../lib/sensors-interface-common.c:156
#, c-format
msgid "%.3f W"
msgstr "%.3f W"

#: ../lib/sensors-interface-common.c:160
msgid "off"
msgstr "isključeno"

#: ../lib/sensors-interface-common.c:160
msgid "on"
msgstr "uključeno"

#: ../lib/sensors-interface-common.c:164
#, c-format
msgid "%.0f rpm"
msgstr "%.0f rpm"

#: ../src/main.c:59
#, c-format
msgid ""
"Xfce4 Sensors %s\n"
"This program is published under the GPL v2.\n"
"The license text can be found inside the program's source archive or under /usr/share/apps/LICENSES/GPL_V2 or at http://www.gnu.org/licenses/old-licenses/gpl-2.0.txt\n"
msgstr "Xfce4 Snzori %s\nOvaj program je izdan pod GPL v2 licencom.\nSadržaj licence možete pronaći unutar izvornog koda programa ili u /usr/share/apps/LICENSES/GPL_V2 ili na http://www.gnu.org/licenses/old-licenses/gpl-2.0.txt\n"

#: ../src/main.c:75
#, c-format
msgid ""
"Xfce4 Sensors %s\n"
"Displays information about your hardware sensors, ACPI status, harddisk temperatures and Nvidia GPU's temperature.\n"
"Synopsis: \n"
"  xfce4-sensors [option]\n"
"where [option] is one of the following:\n"
"  -h, --help    Print this help dialog.\n"
"  -l, --license Print license information.\n"
"  -V, --version Print version information.\n"
"\n"
"This program is published under the GPL v2.\n"
msgstr "Xfce4 Sensors %s\nPrikazuje informacije senzora hardvera, ACPI status, harddisk temperature I Nvidia GPU temperature.\nSinopsis: \n  xfce4-sensors [opcija]\ngdje je [opcija] nešto od sljedećeg:\n  -h, --help    Prikaži okvir s pomoći.\n  -l, --license Prikaži informacije o licenci.\n  -V, --version Prikaži informacije o verziji.\n\nOvaj program je objavljen pod GPL v2 licencom.\n"

#: ../src/main.c:97
#, c-format
msgid "Xfce4 Sensors %s\n"
msgstr "Xfce4 senzori %s\n"

#: ../src/interface.c:76
msgid "_Overview"
msgstr "_Pregled"

#: ../src/interface.c:97
msgid "_Tachometers"
msgstr "_Tahometri"

#: ../src/interface.c:113
msgid "Sensors Viewer"
msgstr "Preglednik senzora"

#. FIXME: either print nothing, or undertake appropriate action,
#. * or pop up a message box.
#: ../src/actions.c:78
#, c-format
msgid ""
"Sensors Viewer:\n"
"Seems like there was a problem reading a sensor feature value.\n"
"Proper proceeding cannot be guaranteed.\n"
msgstr "Preglednik senzora:\nPostoji problem pri čitanju vrijednosti senzora.\nPravilan nastavak nije garantiran.\n"

#: ../src/xfce4-sensors.desktop.in.h:1
msgid "Sensor Viewer"
msgstr "Preglednik senzora"

#: ../src/xfce4-sensors.desktop.in.h:2
#: ../panel-plugin/xfce4-sensors-plugin.desktop.in.h:2
msgid "Show sensor values."
msgstr "Pokaži vrijednosti senzora."

#: ../src/xfce4-sensors.desktop.in.h:3
msgid "Sensor Values Viewer"
msgstr "Preglednik vrijednosti senzora"

#: ../panel-plugin/xfce4-sensors-plugin.desktop.in.h:1
msgid "Sensor plugin"
msgstr "Priključak senzora"
